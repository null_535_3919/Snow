<a href='https://www.aliyun.com/sale-season/2020/procurement-new-members?userCode=6vik1cql' target='_blank'>![image](https://images.gitee.com/uploads/images/2020/0326/235115_448bf4c5_1099285.jpeg)</a>
# JAVA SWING全屏下雪小程序源码


## <a href='http://www.vbox.top?from=oschina' target='_blank'>更多小工具请前往我的博客</a>

# 软件简介 

忘记是哪个圣诞节前夕，看QQ上朋友发的圣诞祝福网页，感觉效果挺好的，那时候还没学会做网站，刚学了点java基础，所以就用java swing写了个全屏下雪的小程序，雪花❉并没有很美![image](https://images.gitee.com/uploads/images/2020/0326/235115_a800bf43_1099285.png)

开发环境：jdk6+

开发工具：netbeans 7.0

效果如下图

![image](https://images.gitee.com/uploads/images/2020/0326/235115_10cce4ef_1099285.jpeg)

# <a href='http://www.vbox.top?from=oschina' target='_blank'>更多小工具请前往我的博客</a>
